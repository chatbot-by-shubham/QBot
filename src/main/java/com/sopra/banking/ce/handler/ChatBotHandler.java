package com.sopra.banking.ce.handler;

import com.sopra.banking.ce.config.ChatBotConfig;
import com.sopra.banking.ce.service.ChatBotService;
import org.alicebot.ab.Bot;
import org.alicebot.ab.Chat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Mono;

import java.io.File;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Component
public class ChatBotHandler {
    private final ChatBotService chatbotService;
    private final ChatBotConfig botConfig;
    private static final String AIML_DIRECTORY = getResourcesPath();
    private final Chat chat;

    @Autowired
    public ChatBotHandler(ChatBotService chatbotService, ChatBotConfig botConfig) {
        this.chatbotService = chatbotService;
        this.botConfig = botConfig;
        Bot bot = new Bot(botConfig.getBotName(), AIML_DIRECTORY);
        chat = new Chat(bot);
    }

    public Mono<String> processMessage(String message) {
        String response = chat.multisentenceRespond(message);
        boolean containsNumber = chatbotService.containsNumberRegex(message);
        if(message.contains(botConfig.getBotName())) {
            response+="\n\nWhat would you like to do today?\n\n" + ChatBotService.getOptions();
        }
        else if(containsNumber){
            Pattern pattern = Pattern.compile("\\D+");
            int actionItem = Integer.parseInt(pattern.splitAsStream(message)
                    .collect(Collectors.joining()));
            return chatbotService.performOperation(actionItem);
        }
        return Mono.just(response);
    }

    private static String getResourcesPath() {
        File currDir = new File(".");
        String path = currDir.getAbsolutePath().substring(0, currDir.getAbsolutePath().length() - 2);
        return path + File.separator + "aiml-directory";
    }
}

