package com.sopra.banking.ce;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = "com.sopra.banking.ce")
public class ChatBotApplication {
    public static void main(String[] args) {
        SpringApplication.run(ChatBotApplication.class, args);
    }
}

