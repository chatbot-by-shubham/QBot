package com.sopra.banking.ce.service;

import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

@Service
public class ChatBotService {

    public Mono<String> performOperation(int input) {
        return Mono.fromCallable(() -> {
            switch (input) {
                case 1:
                    return fetchOperation(input);
                case 2:
                    return addOperation(input);
                case 3:
                    return updateOperation(input);
                case 4:
                    return deleteOperation(input);
                default:
                    return defaultOperation(input);
            }
        });
    }

    public String fetchOperation(int input) {
        // Perform fetch operation
        return input + ": fetched record is  --- To be implemented ";
    }

    public String addOperation(int input) {
        // Perform add operation
        return input + ": add operation is successfully completed ";
    }

    public String updateOperation(int input) {
        // Perform update operation
        return  input + ": update operation is successfully completed ";
    }

    public String deleteOperation(int input) {
        // Perform delete operation
        return input + ": delete operation is successfully completed ";
    }

    public String defaultOperation(int input) {
        // Perform default operation
        return input + " Kindly choose correct output \n\n" + getOptions();
    }

    public boolean containsNumberRegex(String input) {
        return input.chars()
                .anyMatch(Character::isDigit);
    }

    public static String getOptions() {
        return "1. Fetch a record from the timesheet\n"
                + "2. Add a record to the timesheet\n"
                + "3. Update a record in the timesheet\n"
                + "4. Delete a record from the timesheet\n";
    }

}
