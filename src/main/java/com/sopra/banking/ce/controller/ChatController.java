package com.sopra.banking.ce.controller;

import com.sopra.banking.ce.handler.ChatBotHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class ChatController {

    private final ChatBotHandler chatbotHandler;

    @Autowired
    public ChatController(ChatBotHandler chatbotHandler) {
        this.chatbotHandler = chatbotHandler;
    }

    @PostMapping("/chat")
    public Mono<String> chat(@RequestBody String message) {
        return chatbotHandler.processMessage(message);
    }
}

