# QBot (QueryBot)

QBot is a fully operational AI application developed using reactive programming and AIML. It is designed to provide intelligent responses to user queries. This project follows a microservices architecture using Spring Boot and Angular.

## Technologies Used

- IDE: IntelliJ
- Build Tool: Gradle
- Front End: Angular 16.0
- Programming Language: Java 11 (Reactive Programming)
- Application Framework: Spring Boot 2.5.x

## Project Structure

The project follows a microservices architecture, with the following modules:

- `qbot-core`: Core functionality and logic for the chatbot.
- `qbot-api`: RESTful API module for communication with the chatbot.
- `qbot-web`: Front-end module developed with Angular 16.0.
- `qbot-utils`: Utility module with common functionalities.

## Getting Started

To run the QBot application locally, please follow these steps:

### Prerequisites

- Java 11 or higher
- Gradle
- Angular CLI

### Clone the Repository

```bash
git clone https://gitlab.com/chatbot-by-shubham/QBot.git
```

### Backend Setup

1. Open the project in IntelliJ or your preferred IDE.
2. Build the project using Gradle.
3. Run the `QBotApplication` class in the `qbot-api` module to start the API server.

### Frontend Setup

1. Open the `qbot-web` folder in a terminal.
2. Install the project dependencies using the following command:

   ```bash
   npm install
   ```

3. Start the Angular development server using the following command:

   ```bash
   ng serve
   ```

4. Access the application by navigating to `http://localhost:4200` in your web browser.

## Contributing

If you would like to contribute to the QBot project, please follow the guidelines provided in the CONTRIBUTING.md file.


## Contact

For any inquiries or suggestions, please contact the project owner: Shubham Chauhan.

---

Thank you for your interest in QBot! We hope you find it useful and valuable for your AI-based applications.